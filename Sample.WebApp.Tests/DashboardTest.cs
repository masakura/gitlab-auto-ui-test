﻿using System;
using System.Collections;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;

namespace Sample.WebApp.Tests
{
    [TestFixtureSource(typeof(WebDrivers), "All")]
    public sealed class DashboardTest
    {
        private readonly Func<IWebDriver> _factory;
        private IWebDriver _webDriver;

        public DashboardTest(Func<IWebDriver> factory)
        {
            _factory = factory;
        }

        [SetUp]
        public void SetUp()
        {
            _webDriver = _factory();
        }

        [TearDown]
        public void TearDown()
        {
            Thread.Sleep(2000);

            _webDriver.Dispose();
        }

        private static string GetUrl(string path)
        {
            var baseUrl = Environment.GetEnvironmentVariable("WEBAPP_URL") ?? "http://localhost:57925/";
            var url = new Uri(baseUrl);

            return new Uri(url, path).ToString();
        }

        [Test]
        public void ダッシュボード()
        {
            _webDriver.Navigate().GoToUrl(GetUrl("/"));

            var actual = _webDriver.FindElement(By.TagName("h1")).Text;

            Assert.That(actual, Is.EqualTo("Welcome"));
        }

        [Test]
        public void プライバシーポリシー()
        {
            _webDriver.Navigate().GoToUrl(GetUrl("/"));

            _webDriver.FindElement(By.LinkText("Privacy")).Click();

            var actual = _webDriver.FindElement(By.TagName("h1")).Text;

            Assert.That(actual, Is.EqualTo("Privacy Policy"));
        }
    }

    public static class WebDrivers
    {
        public static IEnumerable All
        {
            get
            {
                var seleniumHubUrl = Environment.GetEnvironmentVariable("SELENIUM_HUB_URL");

                if (string.IsNullOrEmpty(seleniumHubUrl))
                {
                    yield return new TestFixtureData((Func<IWebDriver>) (() => new ChromeDriver()));
                    yield break;
                }

                var baseUrl = new Uri(seleniumHubUrl);
                var hubEndpoint = new Uri(baseUrl, "/wd/hub");

                yield return new TestFixtureData((Func<IWebDriver>) (() =>
                    new RemoteWebDriver(hubEndpoint, new ChromeOptions())));
                yield return new TestFixtureData((Func<IWebDriver>) (() =>
                    new RemoteWebDriver(hubEndpoint, new FirefoxOptions())));
            }
        }
    }
}