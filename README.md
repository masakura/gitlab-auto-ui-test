# GitLab CI 自動 UI テスト
自動 UI テストまで組み込まれたサンプルアプリ。[Fork](https://gitlab.com/masakura/gitlab-auto-ui-test/-/forks/new) すればそのまま動きます。多分...


## サンプルアプリの開始
事前に [Git](https://git-scm.com/) と [.NET Core SDK 3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1) [Google Chrome] と (https://www.google.com/chrome/) をインストールしてください。

```
> git clone https://gitlab.com/masakura/gitlab-auto-ui-test.git
> cd gitlab-auto-ui-test
gitlab-auto-ui-test> cd Sample.WebApp
gitlab-auto-ui-test\Sample.WebApp> dotnet run --urls http://localhost:57925/
```

自動テストはサンプルアプリを実行した状態で実行してください。

```
> cd gitlab-auto-ui-test
gitlab-auto-ui-test> cd Sample.WebApp.Tests
gitlab-auto-ui-test\Sample.WebApp.Tests> dotnet test
```
